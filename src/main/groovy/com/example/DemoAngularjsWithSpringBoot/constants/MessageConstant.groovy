package com.example.DemoAngularjsWithSpringBoot.constants

class MessageConstant {
    public static final String  DATA_SAVE_SUCCESS = "Data save successfully."
    public static final String  NO_RECORD_FOUND = "No record found."
}
