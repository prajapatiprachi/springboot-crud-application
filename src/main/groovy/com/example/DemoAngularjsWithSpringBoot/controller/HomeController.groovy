package com.example.DemoAngularjsWithSpringBoot.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class HomeController {

    @RequestMapping("/hello")
    String hello() {
        return "Hello"
    }

}
