package com.example.DemoAngularjsWithSpringBoot.service

import com.example.DemoAngularjsWithSpringBoot.co.EmployeeCO
import com.example.DemoAngularjsWithSpringBoot.dto.ResponseDTO
import com.example.DemoAngularjsWithSpringBoot.entity.Employee

interface EmployeeService {

    List<Employee> getAllEmployee()

    Employee findById(String id)

    ResponseDTO save(EmployeeCO employee)

    Employee update(EmployeeCO employee)

    void deleteById(String id)

}